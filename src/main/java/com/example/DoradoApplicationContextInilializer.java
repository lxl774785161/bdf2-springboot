package com.example;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import com.bstek.dorado.core.CommonContext;
import com.bstek.dorado.core.Context;
import com.bstek.dorado.web.loader.DoradoLoader;

public class DoradoApplicationContextInilializer implements
		ApplicationContextInitializer<ConfigurableApplicationContext> {

	public void initialize(ConfigurableApplicationContext applicationContext) {
		try {
			Context context = CommonContext.init(applicationContext);
			DoradoLoader.getInstance().setFailSafeContext(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
