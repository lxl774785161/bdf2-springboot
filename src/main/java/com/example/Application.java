package com.example;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.bstek.dorado.web.loader.DoradoLoader;
import com.bstek.dorado.web.servlet.DoradoServlet;

@Configuration
@EnableAutoConfiguration
@EnableWebMvc
public class Application {
	@Bean
	public ServletRegistrationBean doradoServlet() {
		ServletRegistrationBean servlet = new ServletRegistrationBean(
				new DoradoServlet());
		servlet.setLoadOnStartup(1);
		servlet.addUrlMappings("*.d");
		servlet.addUrlMappings("*.c");
		servlet.addUrlMappings("*.dpkg");
		servlet.addUrlMappings("/dorado/*");
		return servlet;
	}

	public static void main(String[] args) {
		System.setProperty("doradoHome", "classpath:dorado-home/");
 
		SpringApplication app = new SpringApplication(Application.class);
		app.addInitializers(new DoradoApplicationContextInilializer());

		DoradoLoader doradoLoader = DoradoLoader.getInstance();
		try {
			doradoLoader.preload(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Set<Object> sources = new LinkedHashSet<Object>();
		sources.addAll(doradoLoader.getContextLocations(false));
		app.setSources(sources);

		app.run(args);
	}
}
